# Laptop Service
import os

from flask import Flask, jsonify, request
from flask_restful import Resource, Api
from flask_cors import CORS

from pymongo import MongoClient

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.acp_time

# Instantiate the app
app = Flask(__name__)
api = Api(app)
CORS(app)


def list_dic_to_csv(list_dic, heads):
    csv = ''
    for head in heads:
        csv += head + ', '
        if head == heads[len(heads) - 1]:
            csv = csv[:-2]
            csv += '\n'

    for dic in list_dic:
        for head in heads:
            csv += dic[head] + ', '
            if head == heads[len(heads) - 1]:
                csv = csv[:-2]
                csv += '\n'
    
    return csv

def del_list_dic_key(list_dic, key):
    new_list_dic = []
    for dic in list_dic:
        del dic[key]
        new_list_dic.append(dic)
    return new_list_dic

"""All"""

class acp_time_all(Resource):

    def get(self):
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        controls = control_times['controls']

        return jsonify(result=controls)

class acp_time_all_json(Resource):

    def get(self):
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        controls = control_times['controls']

        return jsonify(result=controls)

class acp_time_all_csv(Resource):

    def get(self):
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        csv_heads = ['miles', 'kms', 'location', 'open', 'close']
        controls_csv = list_dic_to_csv(control_times['controls'], csv_heads)

        return controls_csv

"""OpenOnly"""

class acp_time_openOnly(Resource):

   def get(self):
        top_n = int(request.args['top']) if request.args else None
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        controls = del_list_dic_key(control_times['controls'], 'close')[:top_n]

        return jsonify(result=controls)

class acp_time_openOnly_json(Resource):

    def get(self):
        top_n = int(request.args['top']) if request.args else None
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        controls = del_list_dic_key(control_times['controls'], 'close')[:top_n]

        return jsonify(result=controls)

class acp_time_openOnly_csv(Resource):

    def get(self):
        top_n = int(request.args['top']) if request.args else None
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        csv_heads = ['miles', 'kms', 'location', 'open']
        controls_csv = list_dic_to_csv(del_list_dic_key(control_times['controls'], 'close')[:top_n], csv_heads)
        
        return controls_csv

"""CloseOnly"""

class acp_time_closeOnly(Resource):

    def get(self):
        top_n = int(request.args['top']) if request.args else None
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        controls = del_list_dic_key(control_times['controls'], 'open')[:top_n]
        
        return jsonify(result=controls)

class acp_time_closeOnly_json(Resource):

    def get(self):
        top_n = int(request.args['top']) if request.args else None
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        controls = del_list_dic_key(control_times['controls'], 'open')[:top_n]

        return jsonify(result=controls)

class acp_time_closeOnly_csv(Resource):

    def get(self):
        top_n = int(request.args['top']) if request.args else None
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        csv_heads = ['miles', 'kms', 'location', 'close']
        controls_csv = list_dic_to_csv(del_list_dic_key(control_times['controls'], 'open')[:top_n], csv_heads)
        
        return controls_csv

# Create routes
#part 1
api.add_resource(acp_time_all, '/listAll')
api.add_resource(acp_time_openOnly, '/listOpenOnly')
api.add_resource(acp_time_closeOnly, '/listCloseOnly')

#part 2
api.add_resource(acp_time_all_csv, '/listAll/csv')
api.add_resource(acp_time_openOnly_csv, '/listOpenOnly/csv')
api.add_resource(acp_time_closeOnly_csv, '/listCloseOnly/csv')
api.add_resource(acp_time_all_json, '/listAll/json')
api.add_resource(acp_time_openOnly_json, '/listOpenOnly/json')
api.add_resource(acp_time_closeOnly_json, '/listCloseOnly/json')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)
